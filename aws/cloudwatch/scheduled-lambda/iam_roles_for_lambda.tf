############################
#
# This file creates the "assume role" that is necessary for the Lambda to run, along with
# creating the policy that contains the permissions necessary for the Lambda to do its job.
#
# ECS Permissions    - Needed to get task count
# SQS Permissions    - Needed to get the message count
# CloudWatch Metrics - Needed to put the metric data to CloudWatch
# CloudWatch Logs    - Needed to log to CloudWatch
#
############################

resource "aws_iam_role_policy_attachment" "attachment" {
  policy_arn = aws_iam_policy.lambda_logging.arn
  role = aws_iam_role.iam_for_lambda.name
}

resource "aws_iam_role" "iam_for_lambda" {
  name = "iam-role-for-lambda-${var.function_name}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": [
          "lambda.amazonaws.com",
          "events.amazonaws.com"
        ]
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

# Create the policy doc, then create the policy that uses it
data "aws_iam_policy_document" "doc" {
  statement {
    actions = [
      "ecs:ListServices",
      "ecs:ListAttributes",
      "ecs:ListTasks",
      "ecs:ListTaskDefinitions",
      "ecs:ListClusters",
      "ecs:DescribeServices",

      "sqs:ListQueues",

      "logs:CreateLogStream",
      "logs:PutLogEvents",

      "cloudwatch:PutMetricData"
    ]

    resources = ["*"]
  }
}

resource "aws_iam_policy" "lambda_logging" {
  name = "iam-policy-for-lambda-${var.function_name}"
  path = "/"
  description = "Role Policy for the ${var.function_name} lambda function. Allows logging, access to ECS, SQS, and CloudWatch metrics"

  policy = data.aws_iam_policy_document.doc.json
}

# This always seemed duplicative with the IAM role to me, but it's needed.
resource "aws_lambda_permission" "allow_events" {

  action = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda_function.function_name
  principal = "events.amazonaws.com"

  source_arn = aws_cloudwatch_event_rule.lambda_schedule.arn
}
