############################
#
# This file creates the actual lambda function and it's schedule to run every minute (by default)
#
############################

resource "aws_lambda_function" "lambda_function" {

  function_name = var.function_name

  # You should always include the SHA, otherwise the module won't show and update if you only change the function.
  # That's sort of an important change to get through.
  filename         = var.function_file
  source_code_hash = filebase64sha256(var.function_file)

  handler       = "create-custom-metric.lambda_handler"

  role          = aws_iam_role.iam_for_lambda.arn
  runtime       = "python3.7"

  environment {
    variables = var.environment_variables
  }
}

resource "aws_cloudwatch_event_rule" "lambda_schedule" {
  name = var.function_name
  schedule_expression = var.schedule

  role_arn = aws_iam_role.iam_for_lambda.arn
}

resource "aws_cloudwatch_event_target" "run_lambda" {

  target_id = "cloudwatch-target-for-${var.function_name}"

  arn = aws_lambda_function.lambda_function.arn
  rule = aws_cloudwatch_event_rule.lambda_schedule.name

}

resource "aws_cloudwatch_log_group" "example" {
  name              = "/aws/lambda/${var.function_name}"
  retention_in_days = 1
}


