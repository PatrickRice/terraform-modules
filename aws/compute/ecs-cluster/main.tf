resource "aws_placement_group" "placement_group" {
  name     = "${var.cluster_name}-PlacementGroup"
  strategy = var.placement_strategy

  tags = {
    name = "${var.cluster_name}-PlacementGroup"
    cluster = var.cluster_name
    usage = var.usage
  }
}

module "lifecycle_queue" {
  source = "..\/..\/sqs-queue"

  queue_name = "${var.cluster_name}-Lifecycle-queue"
  usage = var.usage
}

//TODO: We'll replace this with a parameter to pass in eventually.
resource "aws_launch_template" "example_template" {
  name_prefix   = "example"
  image_id      = "ami-098616968d61e549e"
  instance_type = "t2.small"

  instance_market_options {
    market_type = "spot"
  }
}


resource "aws_autoscaling_group" "asg" {
  name                      = "${var.cluster_name}-asg"
  max_size                  = var.max_instances
  min_size                  = var.min_instances
  desired_capacity          = var.desired_instances
  force_delete              = var.force_delete
  placement_group           = aws_placement_group.placement_group.id

  launch_template {
    id      = aws_launch_template.example_template.id
    version = "$Latest"
  }

  vpc_zone_identifier       = ["1", "2"]



  lifecycle {
    ignore_changes = [
      //Ignore any changes to desired, which will happen as part of scaling activities. We don't want to scale
      //down accidently when we deploy something if our desired in TF is lower than our desired in our target env.
      desired_capacity
    ]
  }
}
