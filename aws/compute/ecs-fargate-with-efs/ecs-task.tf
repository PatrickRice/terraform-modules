locals {
  container = [{
    name = var.container-name
    # these are required for version 1.4 fargate tasks, so hard-code them for now.
    family       = "fargate-task-definition"
    networkMode  = "awsvpc"
    memory       = var.container-memory
    cpu          = var.container-cpu
    privileged   = var.container-privileged
    image        = "${var.container-image}:${var.container-tag}"

    environment  = var.container-environment

    secrets      = var.container-secrets

    ulimits = [
      {
        name = "nofile"
        softLimit = var.soft-ulimit-nofile
        hardLimit = var.hard-ulimit-nofile
      }
    ]

    # Map the port on the container straight through to the host, because
    # each fargate container gets its own IP.
    portMappings = [{
      containerPort = var.container-port
      hostPort      = var.container-port
    }]

    mountPoints = [{
      sourceVolume  = var.container-volume-name
      containerPath = var.container-volume-path
      # Figure out a use case to make this configurable later.
      readOnly      = false
    }]

    logConfiguration = {
      # awslogs and firelens are the only two that work with fargate right now.
      logDriver             = "awslogs"
      options =  {
        awslogs-group         = aws_cloudwatch_log_group.fargate-logs.name
        awslogs-region        = data.aws_region.current.name
        awslogs-stream-prefix = var.container-name
      }
    }

    requiresCompatibilities = ["FARGATE"]
  }]
  container_def = jsonencode(local.container)
}

# for the image URL, view the ECR repository terraform
resource "aws_ecs_task_definition" "service" {
  family                = "${var.cluster-name}-${var.usage}"
  container_definitions = local.container_def

  # These two things are required for this to work, so leaving them hard-coded
  requires_compatibilities = ["FARGATE"]
  network_mode = "awsvpc"

  task_role_arn = aws_iam_role.ecs-task-role.arn
  execution_role_arn = aws_iam_role.ecs-task-execution-role.arn

  cpu    = var.container-cpu
  memory = var.container-memory


  volume {
    name = var.container-volume-name

    efs_volume_configuration {
      file_system_id          = aws_efs_file_system.efs-data-drive.id
      root_directory          = var.container-volume-path
      transit_encryption      = "ENABLED"

      authorization_config {
        access_point_id = aws_efs_access_point.efs-access-point.id
        iam             = "ENABLED"
      }
    }
  }
}

resource "aws_cloudwatch_log_group" "fargate-logs" {
  name = "/${var.cluster-name}/app-logs"
  retention_in_days = var.container-log-retention-duration
}

resource "aws_iam_role" "ecs-task-role" {
  name = "ecs-task-role-${var.usage}-for-${var.cluster-name}"
  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
  POLICY

  tags = {
    Name     = "ecs-task-role-${var.usage}-for-${var.cluster-name}"
    Usage    = var.usage
  }
}

resource "aws_iam_policy" "ecs-task-to-efs" {

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
      {
          "Sid": "",
          "Effect": "Allow",
          "Action": [
              "elasticfilesystem:*"
          ],
          "Resource": "*"
      },
      {
          "Effect": "Allow",
          "Action": [
              "ecr:GetAuthorizationToken",
              "ecr:BatchCheckLayerAvailability",
              "ecr:GetDownloadUrlForLayer",
              "ecr:BatchGetImage",
              "logs:CreateLogStream",
              "logs:PutLogEvents",
              "secretsmanager:ListSecretVersionIds",
              "secretsmanager:GetRandomPassword",
              "secretsmanager:GetResourcePolicy",
              "secretsmanager:ListSecrets",
              "secretsmanager:DescribeSecret",
              "secretsmanager:GetSecretValue"
          ],
          "Resource": "*"
      }
  ]
}
  POLICY

}

resource "aws_iam_role_policy_attachment" "ecs-efs-role-attachment" {
  policy_arn = aws_iam_policy.ecs-task-to-efs.arn
  role = aws_iam_role.ecs-task-role.name
}

data "aws_iam_policy" "ecs_task_execution_role_policy" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

resource "aws_iam_role" "ecs-task-execution-role" {

  name = "ecs-task-execution-role-${var.usage}-for-${var.cluster-name}"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
  POLICY

  tags = {
    Name     = "ecs-task-execution-role-${var.usage}-for-${var.cluster-name}"
    Usage    = var.usage
  }
}

resource "aws_iam_role_policy_attachment" "ecs-task-execution-attackment" {
  policy_arn = data.aws_iam_policy.ecs_task_execution_role_policy.arn
  role = aws_iam_role.ecs-task-execution-role.name
}

resource "aws_iam_role_policy_attachment" "ecs-task-execution-secrets-manager" {
  policy_arn = aws_iam_policy.ecs-task-to-efs.arn
  role = aws_iam_role.ecs-task-execution-role.name
}
