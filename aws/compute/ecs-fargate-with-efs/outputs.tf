
output "load-balancer-dns" {
    value = aws_lb.alb.dns_name
}

output "load-balancer-port" {
    value = var.lb-listener-port
}

output "load-balancer-security-group-id" {
    value = aws_security_group.load-balancer-security-group.id
}

output "load-balancer-arn" {
    value = aws_lb.alb.arn
}

output "load-balancer-tg-arn" {
    value = aws_lb_target_group.target-group.arn
}

output "load-balancer-zone-id" {
    value = aws_lb.alb.zone_id
}

output "ecs-cluster-security-group-id" {
    value = aws_security_group.ecs-cluster-security-group.id
}

output "ecs-autoscaling-resource-name" {
    value = "service/${aws_ecs_cluster.cluster.name}/${aws_ecs_service.ecs-service.name}"
}

output "ecs-autoscaling-dimension" {
    value = "ecs:service:DesiredCount"
}

output "ecs-autoscaling-namespace" {
    value = "ecs"
}
