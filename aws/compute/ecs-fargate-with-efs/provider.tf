# This is used to retreve the current region from
# the provider if it's overridden.
data "aws_region" "current" {}
