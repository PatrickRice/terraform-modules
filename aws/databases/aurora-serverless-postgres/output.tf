
output "rds_security_group_id" {
  value = aws_security_group.database_security_group.id
}

output "database_dns" {
  value = aws_rds_cluster.postgresql.endpoint
}

output "database_username" {
  value = aws_rds_cluster.postgresql.master_username
}

output "database_port" {
  value = aws_rds_cluster.postgresql.port
}

output "database_db_name" {
  value = aws_rds_cluster.postgresql.database_name
}

output "database_secret_manager_arn" {
  value = aws_secretsmanager_secret.database_password.arn
}

output "database_secret_manager_id" {
  value = aws_secretsmanager_secret.database_password.id
}

output "database_password" {
  value = aws_rds_cluster.postgresql.master_password
}