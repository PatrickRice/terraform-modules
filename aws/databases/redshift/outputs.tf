
output "redshift_url" {
  value = aws_redshift_cluster.redshift_instance.dns_name
}

output "redshift_parameter_store_name" {
  value = aws_ssm_parameter.redshift_password.name
}

output "redshift_password" {
  value = aws_redshift_cluster.redshift_instance.master_password
}

output "redshift_security_group_id" {
  value = aws_security_group.redshift_security_group.id
}