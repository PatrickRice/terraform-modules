
resource "aws_redshift_cluster" "redshift_instance" {

  cluster_identifier  = var.cluster_name
  database_name       = var.database_name
  master_username     = var.username
  node_type           = var.database_size
  encrypted           = true  # Yep, this is staying hard-coded.
  publicly_accessible = false # This conflicts with the vpc setup if you leave it true

  # Disaster Recovery
  cluster_type       = var.multi_node == true ? "multi-node" : "single-node"
  number_of_nodes    = var.multi_node == true ? var.node_count : "1"

  # Networking
  port               = var.ingress_port

  # Use the password passed in, otherwise generate a new one
  master_password    = local.my_password

  # Roles - concatenates the var additional iam roles onto the created role
  iam_roles          = local.iam_roles

  # Final Snapshot info - only needed when destroying the database
  final_snapshot_identifier = "${var.cluster_name}-final-snapshot"
  skip_final_snapshot       = var.skip_final_snapshot

  # Networking
  cluster_subnet_group_name = aws_redshift_subnet_group.redshift_subnet_group.name
  vpc_security_group_ids = [aws_security_group.redshift_security_group.id]
}

# Restrict the security group to the traffic incoming to the database
resource "aws_security_group" "redshift_security_group" {
  name        = "${var.cluster_name}-sg"
  description = "Allows inbound traffic to redshift cluster ${var.cluster_name}"
  vpc_id      = var.vpc_id

  ingress {
    cidr_blocks = var.ingress_cidr
    from_port   = var.ingress_port
    to_port     = var.ingress_port
    protocol    = "TCP"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}

# The database password, if one isn't provided. This is only used if the passed in password is null.
resource "random_password" "password" {
  length = 16
  special = false
  min_numeric = 1
  min_lower = 1
  min_upper = 1
}

# Create the subnet group for the cluster, using the IDs determined from tags
data "aws_subnet" "subnets" {
  count = length(var.subnet_tag_values)

  vpc_id = var.vpc_id

  filter {
    name = "tag:${var.subnet_tag_name}"
    values = [var.subnet_tag_values[count.index]]
  }
}

resource "aws_redshift_subnet_group" "redshift_subnet_group" {

  name        = var.cluster_name
  description = "Redshift subnet group for ${var.cluster_name}"
  subnet_ids  = data.aws_subnet.subnets.*.id
}

resource "aws_ssm_parameter" "redshift_password" {
  name = "${var.cluster_name}-password"
  type = "SecureString"
  value = local.my_password
}


# This hides the fact that "result" is treated as an unresolved reference in intelliJ
//noinspection HILUnresolvedReference
locals {
  my_password = var.password == "" ? random_password.password.result : var.password
  iam_roles = concat(var.additional_iam_roles, [])
}