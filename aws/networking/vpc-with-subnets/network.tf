##########################
# Outputs from the module
##########################
output "vpc_id" {
  value = aws_vpc.my_vpc.id
}

output "private_subnet_ids" {
  value = [aws_subnet.private_subnets.*.id]
}

output "public_subnet_ids" {
  value = [aws_subnet.public_subnets.*.id]
}

##########################
# Module resources
##########################

# Create a VPC
resource "aws_vpc" "my_vpc" {
  cidr_block = var.vpc_cidr_block

  tags {
    usage = var.usage
  }

}

resource "aws_flow_log" "example" {
  iam_role_arn = aws_iam_role.logging.arn
  log_destination = aws_cloudwatch_log_group.vpc_logging.arn
  traffic_type = var.vpc_flow_log_level
  vpc_id = aws_vpc.my_vpc.id
}

resource "aws_vpc_ipv4_cidr_block_association" "secondary_cidr" {
  count = var.assign_secondary_block ? 1 : 0

  vpc_id     = aws_vpc.my_vpc.id
  cidr_block = var.vpc_secondary_cidr_block
}

resource "aws_subnet" "public_subnets" {
  count = var.public_subnet_count
  vpc_id = aws_vpc.my_vpc.id

  cidr_block = cidrsubnet(aws_vpc.my_vpc.cidr_block, var.subnet_cidr_bit_offset, count.index)

  tags = {
    subnet_name = "public_${count.index}"
    usage = var.usage
  }
}

resource "aws_subnet" "private_subnets" {
  count = var.private_subnet_count
  vpc_id = aws_vpc.my_vpc.id

  # In this case, we add our offset to the in
  cidr_block = cidrsubnet(aws_vpc.my_vpc.cidr_block, var.subnet_cidr_bit_offset, var.public_subnet_offset + count.index)

  tags = {
    subnet_name = "private_${count.index}"
    usage = var.usage
  }
}