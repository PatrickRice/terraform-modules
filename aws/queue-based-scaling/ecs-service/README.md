# ECS-Service, Queue based metric

It's not uncommon to have an application that picks up jobs off a queue, and needs to scale based on
the number of messages within that queue. In AWS, however, this is difficult to do as there is no 
easy metric for when a service should be added or removed. This module will create a custom metric
around how many messages-per-container exist so that target scaling can be used to scale this type 
of application

## Resources Created

This module creates several resources:
 - Lambda function that calculates the ratio of SQS messages in a configurable queue to containers in
 a configurable cluster/service
 - A new CloudWatch metric in the "SQS" namespace
 - A new CloudWatch event rule that schedules the Lambda to run each minute
 - A new IAM role/policy combination granting execution permissions to the Lambda Function
 
Since the Lambda function only uses amazon's API, it doesn't need to reside in the VPC. This means it
both runs faster, and doesn't need to consume an IP address. 

## Example usage

```hcl-terraform
module "lambda_custom_metric" {
  # Reference by Git URL so that these examples work elsewhere if someone wants to use them.
  source = "git::https://gitlab.com/rice-patrick/terraform-modules.git//aws/queue-based-scaling/ecs-service/"

  ecs_cluster_name = ""
  ecs_service_name = ""
  function_name = ""
  sqs_queue_url = ""
}
```
## Variables

See the [variables.tf](./variables.tf) file for defaults and details on variables.

## Modifying Lambda function
In order to modify the lambda function, you need to not only change the file `create-custom-metric.py`,
you will also need to re-zip it and override `create-custom-metric.zip`. This is because Lambda only 
accepts zip files as uploaded functions. To do so, run the following command:

```bash
zip ./create-custom-metric.zip ./create-custom-metric.py
```