

module "schedule_function" {
  source = "git::https://gitlab.com/rice-patrick/terraform-modules.git//aws/cloudwatch/scheduled-lambda/"

  environment_variables = {
    QUEUE_URL    = var.sqs_queue_url
    SERVICE_NAME = var.ecs_service_name
    CLUSTER_NAME = var.ecs_cluster_name
  }

  function_name = var.function_name
  function_file = "${path.module}/create-custom-metric.zip"
  method_name   = "create-custom-metric.lambda_handler"

}