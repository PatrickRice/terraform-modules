variable "function_name" {
  description = "The name of the lambda function."
}
variable "sqs_queue_url" {
  description = "The queue to monitor for messages."
}
variable "ecs_service_name" {
  description = "The service to monitor for container count."
}
variable "ecs_cluster_name" {
  description = "The cluster that contains the service. Unfortunately, can't be inferred from the service name."
}
variable "schedule" {
  description = "How often the lambda should run. Can either be a cron syntax or a 'rate' syntax. See terraform for more."
  default = "rate(1 minute)"
}