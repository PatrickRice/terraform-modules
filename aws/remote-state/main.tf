variable "region" {
  default = "us-east-1"
  description = "The region to use for creating the table and backend buckets. Defaults to us-east-1."
}

provider "aws" {
  region = var.region
}
// Get information about the current account for naming purposes
data "aws_caller_identity" "current" {}

// Create the s3 bucket to hold the remote state
resource "aws_s3_bucket" "state-bucket" {
  bucket = "${data.aws_caller_identity.current.account_id}-terraform-remote-state"
  acl    = "private"

  versioning {
    enabled = true
  }

  logging {
    target_bucket = aws_s3_bucket.state-logging-access-bucket.id
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  tags = {
    Name = "${data.aws_caller_identity.current.account_id}-terraform-remote-state"
  }

}

# Create the bucket to hold access logging for the remote state bucket.
resource "aws_s3_bucket" "state-logging-access-bucket" {
  # We don't need to enable versioning on a logging bucket, so suppress that checkov check.
  #checkov:skip=CKV_AWS_21
  # We don't need to enable access logging on an access log bucket. That'd be hilariously redundant.
  #checkov:skip=CKV_AWS_18

  bucket = "${data.aws_caller_identity.current.account_id}-terraform-remote-state-logging"
  acl    = "log-delivery-write"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  tags = {
    Name = "${data.aws_caller_identity.current.account_id}-terraform-remote-state-logging"
  }
}

# Create the dynamodb table for holding the state lock
resource "aws_dynamodb_table" "state-lock-table" {
  #We don't need to have point-in-time recovery for a table that's only meant to hold data transiently.
  #checkov:skip=CKV_AWS_28

  name = "${data.aws_caller_identity.current.account_id}-terraform-state-lock"
  billing_mode = "PAY_PER_REQUEST"

  hash_key = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }

  tags = {
    Name = "${data.aws_caller_identity.current.account_id}-terraform-state-lock"
  }
}