
resource "aws_kms_key" "a" {
  description             = "KMS Key for ${var.queue_name}"
  deletion_window_in_days = 10
  enable_key_rotation = true
}

resource "aws_sqs_queue" "queue" {
  name = var.queue_name
  delay_seconds = var.delay_seconds
  max_message_size = var.max_message_size
  message_retention_seconds = var.message_retention_seconds
  receive_wait_time_seconds = var.receive_wait_time_seconds

  kms_master_key_id = aws_kms_key.a.id

  tags = {
    name = var.queue_name
    usage = var.usage
  }

}