
provider "aws" {
  region  = "us-east-1"
}

module "create_redshift_for_target" {
  source = "../../aws/databases/redshift"

  skip_final_snapshot = true

  cluster_name = "my-cluster"
  vpc_id = "vpc-5d32053a"
  username = "admin"
}

module "create_dms_cluster" {
  # Reference by Git URL so that these examples work elsewhere if someone wants to use them.
  #source = "git::https://gitlab.com/rice-patrick/terraform-modules.git//aws/databases/dms-cluster/"
  source = "../../aws/databases/dms-cluster/"

  # These roles already exist on the account we're testing with. Skip creating them.
  create_iam_roles = false

  vpc_id = "vpc-5d32053a"
  job_name = "testing-job"

  instance_type = "dms.t2.medium"

  source_endpoint_database_name = "LegalEntityDB"
  source_endpoint_database_url = "legal-entity-db-dev-2-6-1.cwpy6h4olwlv.us-east-1.rds.amazonaws.com"
  source_endpoint_database_username = "hubnextdev"
  source_endpoint_password_path = "/dev-2-6/legal-entity/database/password"

  target_endpoint_database_url = module.create_redshift_for_target.redshift_url
  target_endpoint_database_username = "admin"
  target_endpoint_password = module.create_redshift_for_target.redshift_password
  target_endpoint_password_path = module.create_redshift_for_target.redshift_parameter_store_name

}

resource "aws_security_group_rule" "dms_to_redshift_rule" {
  security_group_id = module.create_dms_cluster.dms_security_group_id

  from_port = 0
  to_port = 0
  protocol = -1 //All traffic

  source_security_group_id = module.create_redshift_for_target.redshift_security_group_id
  type = "ingress"
}

resource "aws_security_group_rule" "redshift_to_dms_rule" {
  security_group_id = module.create_redshift_for_target.redshift_security_group_id

  from_port = 0
  to_port = 0
  protocol = -1 //All traffic

  source_security_group_id = module.create_dms_cluster.dms_security_group_id
  type = "ingress"
}