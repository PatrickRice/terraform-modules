
provider "aws" {
  region  = "us-east-1"
}

module "create_redshift_cluster" {
  # Reference by Git URL so that these examples work elsewhere if someone wants to use them.
  #source = "git::https://gitlab.com/rice-patrick/terraform-modules.git//aws/databases/redshift/"
  source = "../../aws/databases/redshift/"

  cluster_name = "testing-cluster"
  vpc_id = "vpc-5d32053a"

  # Overrides for cheaper testing
  multi_node = false
  skip_final_snapshot = true

}